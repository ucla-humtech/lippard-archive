<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!-- connect to DB here. Pull series title for page title -->
<title><?php echo $_SERVER['QUERY_STRING']; ?></title>
</head>

<body>
<div id="toppart">
<a href="#" onclick="getArticles('all');">View All</a><br />
<?php
	require_once("connectme.php");
	$seriesquery = "SELECT * FROM series";
	$serieses = mysql_query ($seriesquery, $dbh) or die("something's wrong".MYSQL_ERROR);
	//$series = array();
	while ($tempser = mysql_fetch_assoc($serieses)) {
		$series[$tempser['keyname']]['keyname'] = $tempser['keyname'];
		$series[$tempser['keyname']]['title'] = $tempser['title'];
	}
	foreach ($series as $sercar) {
		?><a href="list.php?ser=<?php echo $sercar['keyname']; ?>"><?php echo $sercar['title']; ?></a><br /> <? echo "\n"; } ?>
<hr />
</p>
</div>
<div>
<span id="articleList">
<?php
$journal['spirit']="Spirit of the Times";
$journal['citizen']="The Citizen Soldier";
$months = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
if (isset($_GET['ser'])) {
	$substr = "%".$_GET['ser']."%";
	$get_articles = sprintf("SELECT id, title, pubdate, series, journal FROM articles WHERE series LIKE '%s' ORDER BY pubdate", $substr);
	$articlesglob = mysql_query ($get_articles, $dbh) or trigger_error(mysql_error(),E_USER_ERROR);
	while ($tempart = mysql_fetch_assoc($articlesglob)) {
		$articles[$tempart['pubdate']]['id'] = $tempart['id'];
		$articles[$tempart['pubdate']]['title'] = $tempart['title'];
		$articles[$tempart['pubdate']]['pubdate'] = $tempart['pubdate'];
		$articles[$tempart['pubdate']]['series'] = $tempart['series'];
		$articles[$tempart['pubdate']]['journal'] = $journal[$tempart['journal']];
	}
	
	//assign link argument to a variable to be posted.
	$arg = $_SERVER['QUERY_STRING'];
	//now print everything out ...

?>
<table align="center" width="70%">
	<?php 
	$i = 1;
	foreach ($articles as $artstemp) {?>
		<tr>
			<td><a href="series.php?<?php echo $arg."#".$artstemp['id']; ?>" target="_blank">
				"<?php echo $series[$artstemp['series']]['title'].", No. ".$i.",\" <i>".$artstemp['journal']."</i>, ";
				echo $months[substr($artstemp['pubdate'], 5, 6)-1]." ".substr($artstemp['pubdate'], 8, 9).", ".substr($artstemp['pubdate'], 0, 4).": ".$artstemp['title']; ?>
				</a></td>
		</tr><? $i++; } ?>
</table>
<? } //terminating one huge if statement ?>
</span>
</div>
</body>
</html>
