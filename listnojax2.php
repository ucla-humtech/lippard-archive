<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Select a Series</title>
<script type="text/javascript" language="javascript" src="createrequest.js"> </script>
<script type="text/javascript" language="javascript" src="textutils.js"> </script>
<script type="text/javascript">
<!--
	function getArticles (seriesname) {
		createRequest();
		var url="listgen.php"+"?ser="+seriesname;
	//	alert(url);
		request.open("GET", url, true);
		request.onreadystatechange = updatePage;
		request.send(null);//"?ser="+seriesname);
	}
	
	function updatePage () {
//		alert(request.responseText);
		if (request.readyState==4) {
			var articles = request.responseText;
			var articleListOnPage = document.getElementById("articleList");
			replaceText (articleListOnPage, articles);
		}
	}
-->
</script>

<style type="text/css">
<!--
#bglayer {
	position:absolute;
	left:0px;
	top:0px;
	width:800;
	height:488px;
	z-index:1;
	background-image: url(images/bgimage.jpg);
}
body {
	background-image: url(images/bgimage.jpg);
	background-repeat: repeat-x;
}
#spirit {
	position:absolute;
	left:5px;
	top:117px;
	width:232px;
	height:31px;
	z-index:2;
	font-size: 24px;
	color: #FFFFFF;
	letter-spacing: 2px;
}
#lippard {
	position:absolute;
	left:548px;
	top:85px;
	width:241px;
	height:390px;
	z-index:2;
	letter-spacing: 2em;
}
#citizen {
	position:absolute;
	left:5px;
	top:264px;
	width:277px;
	height:31px;
	z-index:2;
	font-size: 24px;
	color: #FFFFFF;
	letter-spacing: 2px;
}
#titlebar {
	position:absolute;
	left:164px;
	top:18px;
	width:623px;
	height:101px;
	z-index:3;
	font-size: 48px;
	color: #FFFFFF;
	letter-spacing: 2px;
	text-align: right;
}
#citypolice {
	position:absolute;
	left:21px;
	top:152px;
	width:121px;
	height:35px;
	z-index:4;
}
#flib {
	position:absolute;
	left:146px;
	top:152px;
	width:50px;
	height:35px;
	z-index:5;
}
#ourtalisman {
	position:absolute;
	left:200px;
	top:152px;
	height:35px;
	z-index:6;
	width: 145px;
}
#bankcrisis {
	position:absolute;
	left:351px;
	top:152px;
	width:124px;
	height:35px;
	z-index:7;
}
#boz {
	position:absolute;
	left:481px;
	top:152px;
	width:51px;
	height:35px;
	z-index:8;
}
#spbc {
	position:absolute;
	left:21px;
	top:196px;
	width:326px;
	height:35px;
	z-index:9;
}
#satires {
	position:absolute;
	left:352px;
	top:196px;
	width:148px;
	height:35px;
	z-index:10;
}
#mysterious {
	position:absolute;
	left:21px;
	top:313px;
	width:178px;
	height:35px;
	z-index:11;
}
#walnut {
	position:absolute;
	left:206px;
	width:264px;
	height:35px;
	z-index:12;
	top: 313px;
}
#spermaceti {
	position:absolute;
	left:21px;
	top:358px;
	width:234px;
	height:35px;
	z-index:13;
}
#smallcott {
	position:absolute;
	left:262px;
	top:359px;
	width:355px;
	height:35px;
	z-index:14;
}
#misc {
	position:absolute;
	left:21px;
	top:401px;
	width:162px;
	height:35px;
	z-index:15;
}
#articleList {
	position:absolute;
	left:0px;
	top:454px;
	width:794px;
	height:300px;
	z-index:16;
}
a {
	color: #000000;
	text-decoration: none;
}
a:hover {
	background-color: #CCCCCC;
}
-->
</style>
</head>

<body>
<!--<div id="bglayer">
</div>-->
<div id="spirit">Spirit of the Times </div>
<!--<div id="lippard">--><p align="right"><img src="images/lippard.jpg" alt="Picture of George Lippard" width="241" height="390" hspace="0" vspace="72" border="0"></p>
<!--</div>-->
<div id="citizen">The Citizen Soldier </div>
<div id="titlebar">The Early Writings of George Lippard, 1842-43 </div>
<div id="citypolice"><a href="listnojax2.php?ser=cp"><img src="images/citypolice.jpg" alt="City Police" width="121" height="35" border="0"></a></div>
<div id="flib"><a href="listnojax2.php?ser=flib"><img src="images/flib.jpg" alt="Flib" width="50" height="35"></a></div>
<div id="ourtalisman"><a href="listnojax2.php?ser=tali"><img src="images/ourtalisman.jpg" alt="Our Talisman" width="142" height="35"></a></div>
<div id="bankcrisis"><a href="listnojax2.php?ser=bank"><img src="images/bankcrisis.jpg" alt="Bank Crisis" width="124" height="35"></a></div>
<div id="boz"><a href="listnojax2.php?ser=boz"><img src="images/boz.jpg" alt="Boz" width="49" height="35"></a></div>
<div id="spbc"><a href="listnojax2.php?ser=spbc"><img src="images/spbc.jpg" alt="The Sanguine Poetaster/Bread Crust" width="326" height="35"></a></div>
<div id="satires"><a href="listnojax2.php?ser=satire"><img src="images/satires.jpg" alt="Social Satires" width="148" height="35"></a></div>
<div id="mysterious"><a href="listnojax2.php?ser=mysterious"><img src="images/mysterious.jpg" alt="Mysterious Story" width="178" height="36"></a></div>
<div id="walnut"><a href="listnojax2.php?ser=walnut"><img src="images/walnutcoffin.jpg" alt="The Walnut Coffin Papers" width="264" height="36"></a></div>
<div id="spermaceti"><a href="listnojax2.php?ser=sperm"><img src="images/spermaceti.jpg" alt="The Spermaceti Papers" width="234" height="36"></a></div>
<div id="smallcott"><a href="listnojax2.php?ser=absa"><img src="images/brownson.jpg" alt="A. Brownson Smallcott Apologues" width="355" height="34"></a></div>
<div id="misc"><a href="listnojax2.php?ser=misc"><img src="images/misc.jpg" alt="Miscellaneous" width="162" height="34"></a></div>
<div id="articleList">
<?php
	if(isset($_GET['ser'])) { require_once('listgen.php'); } ?>
</div>
</body>
</html>
