<?php require_once 'connectme.php' ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=latin1">
<title>The Early Writings of George Lippard, 1842-1843</title>

<link href="list.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
    function readMore () {
        document.getElementById("rest").style.display = "block";
	document.getElementById("readmore").style.display = "none";
    }
</script>

</head>

<body>
<div id="backgroundwrapper">
<!--div id="intro"><a href="intro.html">A brief introduction and a note on the texts.</a></div-->
<div id="intro"><a href="intro.html"><img src="images/lippardintro.jpg" alt="Introduction and Note on the Texts" /></a></div>
	<?php require("content.php"); ?>
	<div id="spirit">Spirit of the Times </div>
	<div id="citizen">The Citizen Soldier </div>
	<div id="citypolice"><a href="index.php?ser=cp"><img src="images/citypolice.jpg" alt="City Police" width="121" height="35" border="0"></a></div>
	<div id="flib"><a href="index.php?ser=flib"><img src="images/flib.jpg" alt="Flib" width="50" height="35"></a></div>
	<div id="ourtalisman"><a href="index.php?ser=tali"><img src="images/ourtalisman.jpg" alt="Our Talisman" width="142" height="35"></a></div>
	<div id="bankcrisis"><a href="index.php?ser=bank"><img src="images/bankcrisis.jpg" alt="Bank Crisis" width="124" height="35"></a></div>
	<div id="boz"><a href="index.php?ser=boz"><img src="images/boz.jpg" alt="Boz" width="49" height="35"></a></div>
	<div id="spbc"><a href="index.php?ser=spbc"><img src="images/spbc.jpg" alt="The Sanguine Poetaster/Bread Crust" width="326" height="35"></a></div>
	<div id="satires"><a href="index.php?ser=satire"><img src="images/satires.jpg" alt="Social Satires" width="148" height="35"></a></div>
	<div id="mysterious"><a href="index.php?ser=mysterious"><img src="images/mysterious.jpg" alt="Mysterious Story" width="178" height="36"></a></div>
	<div id="walnut"><a href="index.php?ser=walnut"><img src="images/walnutcoffin.jpg" alt="The Walnut Coffin Papers" width="264" height="36"></a></div>
	<div id="spermaceti"><a href="index.php?ser=spermaceti"><img src="images/spermaceti.jpg" alt="The Spermaceti Papers" width="234" height="36"></a></div>
	<div id="smallcott"><a href="index.php?ser=absa"><img src="images/brownson.jpg" alt="A. Brownson Smallcott Apologues" width="355" height="34"></a></div>
	<div id="misc"><a href="index.php?ser=misc"><img src="images/misc.jpg" alt="Miscellaneous" width="162" height="34"></a></div>
</div>
<div id="textwrapper">
	<div id="seriesheader">
		<?php if(isset($_GET['ser'])) {
		    @include "headings/".$_GET['ser'].".html";
		  $q = "SELECT sechead FROM series WHERE keyname='".mysql_real_escape_string($_GET['ser'])."';";
		  $res = mysql_query($q) or die ("Invalid series.".mysql_error());
		  $info = mysql_fetch_assoc($res);?>
		  <p><?php echo $info["sechead"];?> </p>
	</div>
	<div id="articleList">
	<?php include('listgen.php'); 
	
	}?>
	</div>
</div>
<div id="footer">
Site maintained by <a href="http://www.english.ucla.edu/people/facpages.asp?person_id=390">Christopher Looby</a> and <a href="http://www.english.ucla.edu/people/gradpages.asp?person_id=564">David Shepard</a>.
</div>
</body>
</html>
