<?php

	require_once("../connectme.php");
	$query = sprintf("SELECT title FROM series WHERE keyname='%s'", $_GET['ser']);
	$titlegl = mysql_query ($query, $dbh) or die('Didn\'t work. '.mysql_error());
	$title = mysql_fetch_assoc($titlegl);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo $title['title'] ?> Articles</title>
<style>
<!--
p {
	text-indent: 1.5em;
	}
-->
</style>
</head>

<body>
<!--Bredcrumbs-->
<a href="list.php?ser=<?php echo $_GET['ser']; ?>">Back to Article List</a>
<br /><br /><br />

<hr />
<?php
	$substr = "%".$_GET['ser']."%";
	$get_articles = sprintf("SELECT id, title, pubdate, arttext FROM articles WHERE series LIKE '%s' ORDER BY pubdate", $substr);

	$articlesglob = mysql_query ($get_articles, $dbh) or die('Sorry, man. '.mysql_error());

while ($tempart = mysql_fetch_assoc ($articlesglob)) {
	$articles[$tempart['pubdate']]['id'] = $tempart['id'];
	$articles[$tempart['pubdate']]['title'] = $tempart['title'];
	$articles[$tempart['pubdate']]['date'] = $tempart['pubdate'];
	$articles[$tempart['pubdate']]['text'] = $tempart['arttext'];
}
	
//	array_multisort($articles);
	
	//turn string into pieces of text separated by paragraph tags.
function plaintoHTML ($scanned)
{
	$scanned = " <p>".$scanned;
	for (; ; ) {
		$position = strpos ($scanned, "\n");
		$scanned[$position] = ' ';
		if ($position === false) {
			return $scanned."</p>";
			break;
			}
		$scanned = substr($scanned, 0, $position)."</p><p>".substr($scanned, $position);
	}
}
	
foreach ($articles as $tempart) { ?>
	<a name="<?php echo $tempart['id']; ?>" />
	<span class="title">Title: <?php echo $tempart['title']; ?></span><br />
	<span class="date">Originally published on: <?php echo $tempart['date']; ?></span><br />
	<a href="article.php?id=<?php echo $tempart['id']; ?>">Printable view<!--replace with graphic>--></a>
	<br /><br />
	<span class="text">Article Text: <br /><?php echo plaintoHTML($tempart['text']); ?></span>
	<hr />
	<?php } ?>
</body>
</html>