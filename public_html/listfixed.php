<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Select a Series</title>
<script type="text/javascript" language="javascript" src="createrequest.js"> </script>
<script type="text/javascript" language="javascript" src="textutils.js"> </script>
<script type="text/javascript">
<!--
	function getArticles (seriesname) {
		createRequest();
		var url="listgen.php"+"?ser="+seriesname;
		request.open("GET", url, true);
		request.onreadystatechange = updatePage;
		request.send(null);
	}
	
	function updatePage () {
		if (request.readyState==4) {
			var articles = request.responseText;
			var articleListOnPage = document.getElementById("articleList");
			replaceText (articleListOnPage, articles);
		}
	}
-->
</script>
<link href="listfixed.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="holder">
<img src="images/background.jpg" alt="Background" width="800" height="527">
  <div id="spirit">Spirit of the Times </div>
<div id="citizen">The Citizen Soldier </div>
<div id="citypolice"><a href="javascript:getArticles('cp');"><img src="images/citypolice.jpg" alt="City Police" width="121" height="35" border="0"></a></div>
<div id="flib"><a href="javascript:getArticles('flib');"><img src="images/flib.jpg" alt="Flib" width="50" height="35"></a></div>
<div id="ourtalisman"><a href="javascript:getArticles('tali');"><img src="images/ourtalisman.jpg" alt="Our Talisman" width="142" height="35"></a></div>
<div id="bankcrisis"><a href="javascript:getArticles('bank');"><img src="images/bankcrisis.jpg" alt="Bank Crisis" width="124" height="35"></a></div>
<div id="boz"><a href="javascript:getArticles('boz');"><img src="images/boz.jpg" alt="Boz" width="49" height="35"></a></div>
<div id="spbc"><a href="javascript:getArticles('spbc');"><img src="images/spbc.jpg" alt="The Sanguine Poetaster/Bread Crust" width="326" height="35"></a></div>
<div id="satires"><a href="javascript:getArticles('satire');"><img src="images/satires.jpg" alt="Social Satires" width="148" height="35"></a></div>
<div id="mysterious"><a href="javascript:getArticles('mysterious');"><img src="images/mysterious.jpg" alt="Mysterious Story" width="178" height="36"></a></div>
<div id="walnut"><a href="javascript:getArticles('walnut');"><img src="images/walnutcoffin.jpg" alt="The Walnut Coffin Papers" width="264" height="36"></a></div>
<div id="spermaceti"><a href="javascript:getArticles('sperm');"><img src="images/spermaceti.jpg" alt="The Spermaceti Papers" width="234" height="36"></a></div>
<div id="smallcott"><a href="javascript:getArticles('absa');"><img src="images/brownson.jpg" alt="A. Brownson Smallcott Apologues" width="355" height="34"></a></div>
<div id="misc"><a href="javascript:getArticles('misc');"><img src="images/misc.jpg" alt="Miscellaneous" width="162" height="34"></a></div>
<div id="titlebar">The Early Writings of George Lippard, 1842-43 </div>
<div id="articleList"></div>
</div>

</body>
</html>
