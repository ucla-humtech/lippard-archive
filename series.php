<?php

	require_once("connectme.php");
	$valid_seres = array('cp','flib','tali','bank','boz','spbc','satire','absa','mysterious','spermaceti','walnut','misc');
        if (!in_array($_GET['ser'], $valid_seres)) {
            die('Series not recognized');
        } 
	$query = "SELECT title FROM series WHERE keyname='" . mysqli_real_escape_string($dbh, $_GET['ser']) . "'";
	$titlegl = mysqli_query ($dbh, $query) or die('Didn\'t work. '.mysqli_error($dbh));
	$title = mysqli_fetch_assoc($titlegl);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo $title['title'] ?> Articles</title>
<style>
<!--
p {
	text-indent: 1.5em;
	margin:0px;
	}
-->
</style>
</head>

<body>
<!--Bredcrumbs-->
<a href="index.php?ser=<?php echo $_GET['ser']; ?>">Back to Article List</a>
<br /><br /><br />

<hr />
<?php   
    $articlesglob = mysqli_query($dbh, "SELECT * FROM articles WHERE series LIKE '%". mysqli_real_escape_string($dbh, $_GET['ser']) . "%' ORDER BY pubdate");

while ($tempart = mysqli_fetch_assoc ($articlesglob)) {
	$articles[$tempart['msno']]['id'] = $tempart['id'];
	$articles[$tempart['msno']]['title'] = $tempart['title'];
	$articles[$tempart['msno']]['subtitle'] = $tempart['subtitle'];
	$articles[$tempart['msno']]['pubdate'] = $tempart['pubdate'];
	$articles[$tempart['msno']]['text'] = $tempart['arttext'];
	$articles[$tempart['msno']]['series'] = $tempart['series'];
	$articles[$tempart['msno']]['journal'] = $tempart['journal'];
	$articles[$tempart['msno']]['msno'] = $tempart['msno'];
	$articles[$tempart['msno']]['author'] = $tempart['author'];
}

	$seriesquery = "SELECT * FROM series";
	$serieses = mysqli_query ($dbh, $seriesquery) or die("something's wrong".MYSQL_ERROR);
	while ($tempser = mysqli_fetch_assoc($serieses)) {
		$series[$tempser['keyname']] = $tempser['title'];
	}
	
$journal['spirit']="Spirit of the Times";
$journal['citizen']="The Citizen Soldier";
$months = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
	
	//turn string into pieces of text separated by paragraph tags.
function plaintoHTML ($scanned)
{
	$scanned = " <p>".$scanned;
	for (; ; ) {
		$position = strpos ($scanned, "\n");
		$scanned[$position] = ' ';
		if ($position === false) {
			return $scanned."</p>";
			break;
			}
		$scanned = substr($scanned, 0, $position)."</p><p>".substr($scanned, $position);
	}
}
	
foreach ($articles as $tempart) { ?>
	<a name="<?php echo $tempart['id']; ?>" />
	<a href="article.php?id=<?php echo $tempart['id']; ?>">Printable view<!--replace with graphic>--></a>
	<br /><br />
	<span class="text"><?php echo $tempart['text']; ?></span>
	<hr />
	<?php } ?>
</body>
</html>
