<?php

	require_once("connectme.php");
	$query = sprintf("SELECT title FROM series WHERE keyname='%s'", $_GET['ser']);
	$titlegl = mysql_query ($query, $dbh) or die('Didn\'t work. '.mysql_error());
	$title = mysql_fetch_assoc($titlegl);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=latin-1"/> <!--charset=iso-8859-1" /-->
<title><?php echo $title['title'] ?> Articles</title>
<style>
<!--
p {
	text-indent: 1.5em;
	margin:0px;
	}
-->
</style>
</head>

<body>
<!--Bredcrumbs-->
<a href="index.php?ser=<?php echo $_GET['ser']; ?>">Back to Article List</a>
<br /><br /><br />

<hr />
<?php
	$substr = "%".$_GET['ser']."%";
	$get_articles = sprintf("SELECT * FROM articles WHERE series LIKE '%s' ORDER BY pubdate", mysql_real_escape_string($_GET['ser']));

	$articlesglob = @mysql_query ($get_articles, $dbh) or die('Articles not found. '.mysql_error());

while ($tempart = mysql_fetch_assoc ($articlesglob)) {
	$articles[$tempart['msno']]['id'] = $tempart['id'];
	$articles[$tempart['msno']]['title'] = $tempart['title'];
	$articles[$tempart['msno']]['subtitle'] = $tempart['subtitle'];
	$articles[$tempart['msno']]['pubdate'] = $tempart['pubdate'];
	$articles[$tempart['msno']]['text'] = $tempart['arttext'];
	$articles[$tempart['msno']]['series'] = $tempart['series'];
	$articles[$tempart['msno']]['journal'] = $tempart['journal'];
	$articles[$tempart['msno']]['msno'] = $tempart['msno'];
	$articles[$tempart['msno']]['author'] = $tempart['author'];
}

	$seriesquery = "SELECT * FROM series";
	$serieses = mysql_query ($seriesquery, $dbh) or die("something's wrong".MYSQL_ERROR);
	while ($tempser = mysql_fetch_assoc($serieses)) {
		$series[$tempser['keyname']] = $tempser['title'];
	}
	
$journal['spirit']="Spirit of the Times";
$journal['citizen']="The Citizen Soldier";
$months = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
	
	//turn string into pieces of text separated by paragraph tags.
function plaintoHTML ($scanned)
{
	$scanned = " <p>".$scanned;
	for (; ; ) {
		$position = strpos ($scanned, "\n");
		$scanned[$position] = ' ';
		if ($position === false) {
			return $scanned."</p>";
			break;
			}
		$scanned = substr($scanned, 0, $position)."</p><p>".substr($scanned, $position);
	}
}
	
foreach ($articles as $tempart) { ?>
	<a name="<?php echo $tempart['id']; ?>" />
	<a href="article.php?id=<?php echo $tempart['id']; ?>">Printable view<!--replace with graphic>--></a>
	<br /><br />
	<span class="text"><?php echo $tempart['text']; ?></span>
	<hr />
	<?php } ?>
</body>
</html>
