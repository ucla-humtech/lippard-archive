<html>
<head>
<title>The Early Writings of George Lippard, 1842-43 -- A Brief Introduction and A Note on the Texts</title>
</head>
<body>
<center><b>A Brief Introduction and a Note on the Texts</b></center>
<center><b>Christopher Looby</b></center>
<br/>
<p>When George Lippard&rsquo;s astonishing 
novel, <i>The Quaker City</i>, appeared in ten installments in late 
1844 and early 1845, its bold ambition and stunning accomplishment might 
not have been expected from this fairly obscure Philadelphia journalist 
and neophyte fiction writer.  Just 22 years old at the time, Lippard 
(1822-54) had previously published just one short story, &ldquo;Philippe 
de Agramont,&rdquo; a rather turgid medieval Gothic tale, and a short serial 
novel, &ldquo;Herbert Tracy,&rdquo; a romance set against the backdrop of the 
American Revolution.  Neither of these fictions would have prepared 
readers or critics for the massive scale and startling originality of <i>
The Quaker City</i>, with its richly detailed portrait of life in Philadelphia 
and its unstinting critique of social injustice and moral chicanery.  
Where, then, did Lippard come from, and how did his astonishing literary 
talent develop?  Lippard had also served a brief apprenticeship 
as a writer for two local newspapers, first <i>The Spirit of the Times</i> 
(a radical democratic penny paper) and then <i>The Citizen Soldier</i> 
(a weekly paper affiliated with the volunteer militia movement).  
Very few of his contributions to these two newspapers have been collected 
and reprinted, nor have they been studied very extensively, but they 
provide the best evidence for understanding the fast and dramatic development 
of Lippard&rsquo;s talent and the remarkably quick growth of his ambition.  
The present anthology collects and publishes a large selection of these 
early writings&mdash;including, for the first time, the complete texts of 
the most important of them, the several organized series of sketches 
that best exhibit his swift passage from short-form journalist to long-form 
fiction writer.</font> <br></p>
<p>Lippard&rsquo;s early life is not particularly 
well documented.  Born in 1822 in West Nantmeal Township, Chester 
County, Pennsylvania, he spent most of the early years of his childhood 
in Germantown on his paternal family&rsquo;s farmstead, raised chiefly by 
two aunts and his German-speaking grandfather.  He had been left 
(with his sisters) in their care when his parents moved to Philadelphia, 
having found themselves unable to care for a large family in their reduced 
circumstances.  By the time the aunts sold the Germantown farm 
and themselves moved to Philadelphia in 1832, Lippard&rsquo;s mother had 
died and his father had remarried, but he and his sisters never lived 
with their father and stepmother.  Between 1830 and 1843, Lippard 
lost not only his mother, but his grandfather, his father, his infant 
brother, and two sisters.  (Having endured so many deaths in his 
youth, he would repeat the experience in adulthood, losing his sister 
Harriet in 1848, his infant son Paul in 1851, and his 26-year-old wife 
Rose in that same year; Lippard himself died in 1854 of tuberculosis, 
just short of his thirty-second birthday.)  He left Philadelphia 
and attended a classical school in Rhinebeck, New York, briefly in 1837, 
with the idea of preparing for the Methodist ministry, but this vocational 
goal did not endure long.  Returning to Philadelphia, he soon began 
working as a legal assistant for William Badger, a lawyer, and then 
for Ovid F. Johnson, the Attorney-General of Pennsylvania. He lived 
sometimes with his Aunt Mary, but according to his earliest (and perhaps 
somewhat unreliable) biographer, John Bell Bouton, he established squatter&rsquo;s 
dominion for a time in an abandoned mansion near Franklin Square and 
lived a penurious bohemian life. What seems clear inferentially from 
his earliest writings, collected here, is that he circulated energetically 
among the eager and irreverent wiseasses of the city, and knew his way 
around the streets and alleys, newsrooms and courtrooms, dark oyster 
cellars and crowded hotel lobbies, cold artists&rsquo; garrets and bare 
studios, noisy commercial markets and brilliant gaslit theaters of Philadelphia: 
the public spaces where he encountered and became part of the city&rsquo;s 
rambunctious and irreverent plebeian culturati. This is the lively milieu 
in which he set his earliest writings, and in which he developed his 
expressive style and distinctive approach.</font> <br></p>

<p>A small number of the writings presented 
here were recovered and reprinted (sometimes in incomplete form) by 
the pioneering Lippard scholar David S. Reynolds, in <i>George 
Lippard, Prophet of Protest: Writings of an American Radical, 1822-1854</i> 
(New York, Berne, Frankfurt am Main: Peter Lang, 1986).  They can 
be found interspersed therein among the several thematically-organized 
sections of that essential anthology.  But the majority of the 
writings collected by Reynolds in <i>Prophet of Protest</i> date from 
later in Lippard&rsquo;s career, after his political ideas had developed, 
his literary predilections had formed, and his notoriety had been established.  
A comprehensive representation of Lippard&rsquo;s earliest efforts, from 
the intense 1842-43 period when he emerged as a writer, has not been 
readily available, even to devoted scholars.  Indeed, the two newspapers 
in question are both rare and relatively inaccessible&mdash;especially <i>
The Citizen Soldier</i>, which evidently survives as a complete run 
in only one deteriorating copy, held by the Spruance Library of the 
Bucks County Historical Society in Doylestown, Pennsylvania.  (Happily, 
this periodical was microfilmed at the instigation of the present editor 
during the preparation of this anthology, and a copy of that microfilm 
is now also available at the Library Company of Philadelphia for interested 
students of Lippard.)  In the interest of making a more inclusive 
selection from this fascinating archive of early Lippard writings freely 
available to fellow scholars and other interested readers, it is presented 
here in what I hope will be a simple and user-friendly format.  
(Let me here express my deep appreciation to David Shepard for his elegant 
website design and astute assistance with this project.)</font> <br>
</p>
<p>Because a good deal of the interest 
attaching to this early period in Lippard&rsquo;s career lies in his development&mdash;from 
daily journalism driven by chance events to longer-form writings driven 
by his growing confidence and ambition&mdash;the writings are organized 
here in several series (A-M), conforming to discernible thematic and 
formal threads in his own literary production, and the contents of each 
series is presented chronologically.  Series A (&ldquo;City Police&rdquo;) 
contains a representative selection from Lippard&rsquo;s daily contributions 
which appeared in <i>The Spirit of the Times</i> under that heading; 
there are many more &ldquo;City Crimes&rdquo; entries that researchers should 
seek out.  Likewise, Series M (&ldquo;Miscellaneous&rdquo;) contains a 
selection of writings from <i>The Citizen Soldier</i> that may be attributed 
with some confidence to Lippard, but there are doubtless other pieces 
that scholars can judge for themselves in their original context.  
The major series, however, that constitute Lippard&rsquo;s self-training 
in narrative art (Series C, &ldquo;Our Talisman,&rdquo; Series F, &ldquo;The Sanguine 
Poetaster/Bread Crust Papers,&rdquo; Series K, &ldquo;The Spermaceti Papers,&rdquo; 
and Series L, &ldquo;The Walnut Coffin Papers&rdquo;), are all presented here 
in complete form for the first time.</font> <br></p>

<p>Broadly speaking, Lippard began his 
writing career at a crucial moment in a historical transition of the 
print public sphere, from what J&uuml;rgen Habermas described as a journalism 
of political commitment to a more commercially-driven journalism governed 
by the desire to maximize circulation and sales by appealing to a mass 
readership across the spectrum of political opinion. Beginning his writing 
career in Philadelphia in the early 1840s, Lippard found this historical 
transition hard to navigate.  This is a transition that Lippard 
observed and deplored, as will be obvious to readers of the writings 
collected here: as his democratic political indignation grew, and his 
desire to push the boundaries of middlebrow taste intensified, Lippard 
was increasingly at odds with the trend among newspapers and magazines 
toward political neutrality, saccharine &ldquo;morality,&rdquo; and sentimental 
protocols of tasteful expression, all personified for him in the person 
of George R. Graham, the leading Philadelphia publisher of successful 
mass-market periodicals. Although Lippard had published a few things 
in Graham periodicals early on, his political rambunctiousness and literary 
sensationalism soon made him persona non grata in the respectable publishing 
precincts occupied by Graham and his ilk.  Hence he made a virtue 
of necessity, eluding the makers of taste and monitors of morality in 
Philadelphia by self-publishing his novel <i>The Quaker City</i>, and 
enjoying its runaway popular success immensely (sneering in its preface 
at those by whom &ldquo;it has been denounced as the most immoral work of 
the age&rdquo;). Likewise, he soon thereafter founded his own weekly newspaper, 
also titled <i>The Quaker City</i>, over which he had editorial control 
during its run from December 1848 to June 1850 (a healthy selection 
from its contents can be found in Reynolds&rsquo; anthology).  The 
mainstream periodical press continued on its journey toward political 
innocuousness, and publishers of literature (as well as many readers) 
tended more and more to favor what Lippard jeeringly scorned as &ldquo;ginger-pop 
poets, and root-beer rhymsters&rdquo; (K2, below), and &ldquo;the Bombazine 
School of Literature&rdquo; (K6).  &ldquo;Mediocrity is the order of the 
day,&rdquo; he lamented; &ldquo;We have <i>mediocre</i> novels, <i>mediocre</i> 
tales, <i>mediocre</i> poetry, <i>mediocre</i> essays, and <i>mediocre</i> 

wit,&rdquo; he complained.  But Lippard fought hard against this tendency, 
as the writings collected here testify, and he strove arduously to provide 
something distinctly other than the &ldquo;imbecility&rdquo; and &ldquo;pseudo-morality&rdquo; 
of the &ldquo;emasculated productions&rdquo; (M7) he saw around him. </font> <br>
</p>
<p>A NOTE ON THE TEXTS: To the degree 
possible the texts have been reproduced here just as they appeared in <i>
The Spirit of the Times</i> and <i>The Citizen Soldier</i>, and without 
annotation or other unnecessary editorial apparatus.  It seems 
best to allow readers to encounter these writings without a great deal 
of interference.  Brief headnotes provide a minimum of relevant 
context.  There has been no comprehensive regularization or modernization 
of the texts, but because the typography of both of the original newspapers 
was plainly imperfect, some missing punctuation has been silently supplied, 
some obvious misspellings have been corrected, contractions have been 
made consistent (&ldquo;hadn&rsquo;t&rdquo; and &ldquo;wouldn&rsquo;t&rdquo; rather than the 
occasional &ldquo;had&rsquo;nt&rdquo; and &ldquo;would&rsquo;nt&rdquo;), and the use of em- 
and en-dashes has been regularized.  </font> <br> <br>
</p>
